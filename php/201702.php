<!DOCTYPE html>
<html>
<head>
    <title>Spider-Man</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
    <div class="container">
        <?php
            include_once('../html/nav.html');
        ?>
        <div class="right">
            <div class="section">
                <!-- video info -->
                <img src="../img/201702.jpg"/>
                <div class="videoinfo">
                    <h1 class="name">Spider-Man: Homecoming</h1>
                    <p class="year">2017</p>
                    <p class="director">Directors: Jon Watts</p>
                    <p class="details">Thrilled by his experience with the Avengers, young Peter Parker returns home to live with his Aunt May. Under the watchful eye of mentor Tony Stark, Parker starts to embrace his newfound identity as Spider-Man. He also tries to return to his normal daily routine -- distracted by thoughts of proving himself to be more than just a friendly neighborhood superhero. Peter must soon put his powers to the test when the evil Vulture emerges to threaten everything that he holds dear.</p>
                </div>
            </div>
            <?php
                include_once('../html/footer.html');
            ?>
        </div>
    </div>
</body>
</html>