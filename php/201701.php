<!DOCTYPE html>
<html>
<head>
	<title>
    Despicable Me
    </title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>

    <div class="container">
        <?php
            include_once('../html/nav.html');
        ?>
        <div class="right">
            <div class="section">
                <!-- video info -->
                <img src="../img/201701.jpg"/>
                <div class="videoinfo">
                    <h1 class="name">Despicable Me 3</h1>
                    <p class="year">2017</p>
                    <p class="director">Directors:  Kyle Balda, Pierre Coffin</p>
                    <p class="details">Gru meets his long-lost charming, cheerful, and more successful twin brother Dru who wants to team up with him for one last criminal heist.</p>
                </div>
            </div>
            <?php
                include_once('../html/footer.html');
            ?>
        </div>
    </div>
</body>
</html>