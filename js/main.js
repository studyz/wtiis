
function checkName(username){  
    var usernameFormat = /^[a-zA-Z]\w+$/;
    if(username != "" && usernameFormat.test(username)){ 
        // !username.value.match(usernameFormat)){ 
        return true;
    }else{
        return false;
    }
  }

function checkEmail(email) {
    var emailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (emailFormat.test(email)) {
        return true;
    } else {
        return false;
    }
}

function checkPassw(passw1,passw2){
    if(passw1 == "" || passw2 == "" || passw1 !== passw2){ 
        return false;
    }else{
        return true;
    }
  }

function check(){
    var errorInfo = "";
    var nameOk = false;
    var emailOk = false;
    var passwOk = false;
    
    if(checkName(document.joinForm.username.value)){ 
        nameOk = 1; 
    } else {
        document.joinForm.username.focus();
        if (errorInfo != ""){
            errorInfo = errorInfo + "\n"
        }

        errorInfo = errorInfo + "You have entered an invalid username!";
    }

    if(checkEmail(document.joinForm.email.value)){ 
        emailOk += 1; 
    } 
    else {
        document.joinForm.email.focus();
        if (errorInfo != ""){
            errorInfo = errorInfo + "\n"
        }
        errorInfo = errorInfo + "You have entered an invalid email address!";
    }

    if(checkPassw(document.joinForm.passwd1.value,document.joinForm.passwd2.value)){ 
        passwOk = 1; 
    } else{
        document.joinForm.passwd1.focus();
        document.joinForm.passwd2.focus();
        if (errorInfo != ""){
            errorInfo = errorInfo + "\n"
        }
        errorInfo = errorInfo + "You have entered different passwords!";
    } 

    if(nameOk == 1 && passwOk == 1 && emailOk == 1){ 
        alert("Register Success!!");
        return true; 
    } else {
        alert(errorInfo);
        return false; 
    }
     
  }