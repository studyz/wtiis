<!DOCTYPE html>
<html>
<head>
	<title>Wonder Woman</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
    <div class="container">
        <?php
            include_once('../html/nav.html');
        ?>
        <div class="right">
            <div class="section">
                <!-- video info -->
                <img src="../img/201705.jpg"/>
                <div class="videoinfo">
                    <h1 class="name">Wonder Woman</h1>
                    <p class="year">2017</p>
                    <p class="director">Directors: Rupert Sanders</p>
                    <p class="details">Before she was Wonder Woman (Gal Gadot), she was Diana, princess of the Amazons, trained to be an unconquerable warrior. Raised on a sheltered island paradise, Diana meets an American pilot (Chris Pine) who tells her about the massive conflict that's raging in the outside world. Convinced that she can stop the threat, Diana leaves her home for the first time. Fighting alongside men in a war to end all wars, she finally discovers her full powers and true destiny.</p>
                </div>
            </div>
            <?php
                include_once('../html/footer.html');
            ?>
        </div>
    </div>
</body>
</html>