<!DOCTYPE html>
<html>
<head>
	<title>The Mummy</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    
</head>
<body>
    <div class="container">
        <?php
            include_once('../html/nav.html');
        ?>
        <div class="right">
            <div class="section">
                <!-- video info -->
                <img src="../img/201704.jpg"/>
                <div class="videoinfo">
                    <h1 class="name">The Mummy</h1>
                    <p class="year">2017</p>
                    <p class="director">Directors: Rupert Sanders</p>
                    <p class="details">Nick Morton is a soldier of fortune who plunders ancient sites for timeless artifacts and sells them to the highest bidder. When Nick and his partner come under attack in the Middle East, the ensuing battle accidentally unearths Ahmanet, a betrayed Egyptian princess who was entombed under the desert for thousands of years. With her powers constantly evolving, Morton must now stop the resurrected monster as she embarks on a furious rampage through the streets of London.</p>
                </div>
            </div>
            <?php
                include_once('../html/footer.html');
            ?>
        </div>
    </div>

</body>
</html>