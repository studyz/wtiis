<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Join Us</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body onload='document.userForm.email.focus()'>
    <div class="container">
        <?php
            include_once('../html/nav.html');
        ?>
        <div class="right">
            <div class="section">

                <div class="joinside">
                    <form name="joinForm" method="post" action="saveRecord.php" 
                    onsubmit="return check()">
                        <!-- onclick="return check()"> -->
                        <div class="joinform">
                            <header>Join us</header>

                            <input type="text" class="username" name="username" placeholder="Username" required="required" maxlength="20"/>

                            <input type="email" class="email" name="email" placeholder="Email" required="required" maxlength="50"/>

                            <input type="password" class="passwd1" name="passwd1" placeholder="Password" required="required" maxlength="20" />

                            <input type="password" class="passwd2" name="passwd2" placeholder="Password again" required="required" maxlength="20"/>
                            <span>I have read and agree to the <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a></span>
                            <button type="submit" class="joinbutton" name="submit">Create Account</button>
                        </div>
                    </form>
                
                </div>
                <div class="tableside">
                    <?php
                         include "dataConnector.php";
                         $conn = databaseConnector();
                        // if (isset($_POST['submit'])) {
                        //  sql statement
                        $select = "select * from user";
                        //  run sql and add data into database
                        $select_result = $conn->query($select);
                    ?>
                    <table class="userlist">
                    <thead>
                        <tr>
                        <td><b>ID</b></td>
                        <td><b>username</b></td>
                        <td><b>email</b></td>
                        <td><b>password</b></td>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        if ($select_result->num_rows > 0) {
                            while($row = $select_result->fetch_assoc()) {  
                            ?> 
                    <tr>
                    <td><?php echo $row["id"]; ?></td>
                    <td><?php echo $row["username"]; ?></td>
                    <td><?php echo $row["email"]; ?></td>
                    <td><?php echo $row["password"]; ?></td>
                    </tr>
                    <?php
                            }
                        } else {
                            echo "not found!";
                        }
                        $conn->close();
                    ?>
                    </tbody>
                    </table>
                </div>
            </div>
            <?php
                include_once('../html/footer.html');
            ?>
        </div>
    </div>
<script src="../js/main.js"></script>
</body>
</html>