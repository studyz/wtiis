<!DOCTYPE html>
<html>
<head>
	<title>Transformers</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
    <div class="container">
          <?php
              include_once('../html/nav.html');
          ?>
          <div class="right">
              <div class="section">
                  <!-- video info -->
                  <img src="../img/201703.jpg"/>
                  <div class="videoinfo">
                      <h1 class="name">Directors: Rupert Sanders</h1>
                      <p class="year">2017</p>
                      <p class="director">Directors:  Kyle Balda, Pierre Coffin</p>
                      <p class="details">Humans are at war with the Transformers, and Optimus Prime is gone. The key to saving the future lies buried in the secrets of the past and the hidden history of Transformers on Earth. Now, it's up to the unlikely alliance of inventor Cade Yeager, Bumblebee, an English lord and an Oxford professor to save the world.</p>
                  </div>
              </div>
              <?php
                  include_once('../html/footer.html');
              ?>
          </div>
      </div>

</body>
</html>