<!DOCTYPE html>
<html>
<head>
	<title>Videos</title>

    <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>

    <div class="container">
        <?php
            include_once('../html/nav.html');
        ?>
        <div class="right">
            <div class="section">
                <?php
                    include_once('dataConnector.php');
                    $conn = databaseConnector();
                    $query = "SELECT * FROM `movie`";
                    $result = mysqli_query($conn,$query);
                ?>

                <table class="videolist">

                <thead>
                <tr>
                <th>ID</th>
                <th>Moviename</th>
                <th>Year</th>
                <th>Directors</th>
                </tr>

                </thead>

                <tbody>


                <?php

                    if (mysqli_num_rows($result) > 0) {
                        while($row = mysqli_fetch_assoc($result)) {
                ?>


                <tr>
                    <td><?php echo $row["id"]; ?></td>
                    <td>

                        <?php echo "<a href='../php/".$row["id"].".php'>"; ?>
                        <?php echo $row["moviename"]; ?>
                        </a>
                    </td>
                    <td><?php echo $row["movieyear"]; ?></td>
                    <td><?php echo $row["moviedirectors"]; ?></td>
                </tr>


                <?php
                            
                            } 
                    } else {
                        echo "<h1>0 results</h1>";
                    }
                    mysqli_close($conn);
                ?>

                </tbody>
                </table>
            </div>
            <?php
                include_once('../html/footer.html');
            ?>
        </div>
    </div>

</body>
</html>