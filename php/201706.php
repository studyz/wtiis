<!DOCTYPE html>
<html>
<head>
	<title>Ghost in the Shell</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
    <div class="container">
        <?php
            include_once('../html/nav.html');
        ?>
        <div class="right">
            <div class="section">
                <!-- video info -->
                <img src="../img/201706.jpg"/>
                <div class="videoinfo">
                    <h1 class="name">Ghost in the Shell</h1>
                    <p class="year">2017</p>
                    <p class="director">Directors: Rupert Sanders</p>
                    <p class="details">In the near future, Major is the first of her kind: a human who is cyber-enhanced to be a perfect soldier devoted to stopping the world's most dangerous criminals. When terrorism reaches a new level that includes the ability to hack into people's minds and control them, Major is uniquely qualified to stop it. As she prepares to face a new enemy, Major discovers that her life was stolen instead of saved. Now, she will stop at nothing to recover her past while punishing those who did this to her.</p>
                </div>
            </div>
            <?php
                include_once('../html/footer.html');
            ?>
        </div>
    </div>
</body>
</html>