<!DOCTYPE html>
<html>
<head>
	<title>Contact us!</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
    <div class="container">
            <?php
                include_once('../html/nav.html');
            ?>
        <div class="right">
                <div class="section">
                    <div class="contact">
                        <h1 class="contact">Contact us:</h1>
                        <p>
                            <ul>
                                <li>Name: Xue Yang</li>
                                <li>Email:18942239@westernsydney.edu.au</li>
                                <li>This Project is for WSU 2017 s2 Web Tech</li>
                            </ul>
                        </p>
                    </div>
                </div>
            

            <?php
                include_once('../html/footer.html');
            ?>
        </div>
    </div>
</body>
</html>