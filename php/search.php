<!--
 * Created by PhpStorm.
 * User: studyz
 * Date: 18/7/17
 * Time: 19:00
-->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Videos</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css"> 
</head>
<body onload='document.userForm.email.focus()'>
    <div class="container">
            <?php
                include_once('../html/nav.html');
            ?>
        <div class="right">
            <div class="section">
                <?php
                    include_once('dataConnector.php');
                    $conn = databaseConnector();
                    // If the values are posted, insert them into the database.
                    if (isset($_POST['keyword'])) {
                        $keyword = $_POST['keyword'];

                        // echo $username,$email,$passwd;
                        $query = "SELECT * FROM `movie`";
                        // $query = "INSERT INTO `user` (username, email,password, active) VALUES ('$username','$email','$passwd','0')";
                        $result = mysqli_query($conn, $query);
                        // if ($result) {
                        //     $smsg = "User Created Successfully.";
                        // } else {
                        //     $fmsg = "User Registration Failed";
                        // }
                    } else {
                        echo "nothing passed!!";
                    }
                ?>


                <table class="videolist">

                 <thead>
                <tr>
                <th>ID</th>
                <th>Moviename</th>
                <th>Year</th>
                <th>Directors</th>
                </tr>

                </thead>

                <tbody>
                <?php
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) {
                            if (strpos(strtolower($row["moviename"]), strtolower($keyword))) {
                                 
                ?>
                <tr>
                <td><?php echo $row["id"]; ?></td>
                <td>
                <?php echo "<a href='../php/".$row["id"].".php'>"; ?>
                        <?php echo $row["moviename"]; ?>
                        </a>
                </td>
                <td><?php echo $row["movieyear"]; ?></td>
                <td><?php echo $row["moviedirectors"]; ?></td>
                </tr>
                <?php
                            }
                        } 
                    } else {
                        echo "<p>not found!</p>";
                    }
                    $conn->close();
                ?>

                </tbody>
                </table>
            </div>
        

            <?php
                include_once('../html/footer.html');
            ?>
        </div>
    </div>
</body>
</html>
